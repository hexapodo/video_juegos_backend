import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { EmpresaDesarrolladora } from '../entity/EmpresaDesarrolladora';
import { Plataforma } from '../entity/Plataforma';

export class EmpresaController {

    static getEmpresasDesarrolladoras = async (req: Request, res: Response): Promise<Response> => {
        const empresasDesarrolladoras = await getRepository(EmpresaDesarrolladora).find({relations: ['videoJuegos', 'videoJuegos.plataforma']});
        return res.json(empresasDesarrolladoras);
    }

    static getEmpresaDesarrolladora = async (req: Request, res: Response): Promise<Response> => {
        const empresaDesarrolladora = await getRepository(EmpresaDesarrolladora).findOne(req.params.id,{relations: ['videoJuegos', 'videoJuegos.plataforma']});
        if (empresaDesarrolladora) {
            return res.json(empresaDesarrolladora);
        }
        return res.status(404).json({ msg: 'Empresa desarrolladora no encontrada' });
    }

    static getEmpresasDesarrolladorasByPlataforma = async (req: Request, res: Response): Promise<Response> => {
        const plataforma = await getRepository(Plataforma).findOne(req.params.id, {
            relations: [
                'videoJuegos',
                'videoJuegos.empresaDesarrolladora',
                'videoJuegos.empresaDesarrolladora.videoJuegos'
            ]
        });
        if (!plataforma) {
            res.status(404);
        }
        const empresas = plataforma?.videoJuegos.map(videoJuego => videoJuego.empresaDesarrolladora);
        const empresasUnicas = [];
        const map = new Map();
        if (empresas) {
            for (const empresa of empresas) {
                if(!map.has(empresa.id)){
                    map.set(empresa.id, true);
                    empresasUnicas.push(empresa);
                }
            }
        }
        return res.json(empresasUnicas);
    }



}


