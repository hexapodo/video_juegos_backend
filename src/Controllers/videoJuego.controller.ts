import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { VideoJuego } from '../entity/VideoJuego';
import { EmpresaDesarrolladora } from '../entity/EmpresaDesarrolladora';
import { Factura } from '../entity/Factura';
import { User } from '../entity/User';
import { Registro } from '../entity/Registro';

export class VideoJuegoController {

    static getVideoJuegos = async (req: Request, res: Response): Promise<Response> => {
        const videoJuegos = await getRepository(VideoJuego).find({relations: ['empresaDesarrolladora', 'plataforma']});
        return res.json(videoJuegos);
    }

    static getVideoJuego = async (req: Request, res: Response): Promise<Response> => {
        const videoJuego = await getRepository(VideoJuego).findOne(req.params.id,{relations: ['empresaDesarrolladora', 'plataforma']});
        if (videoJuego) {
            return res.json(videoJuego);
        }
        return res.status(404).json({ msg: 'Video juego no encontrado' });
    }

    static createVideoJuego = async (req: Request, res: Response): Promise<Response> => {
        const newVideoJuego = getRepository(VideoJuego).create(req.body);
        const results = await getRepository(VideoJuego).save(newVideoJuego);
        return res.json(results);
    }

    static modifyVideoJuego = async (req: Request, res: Response): Promise<Response> => {
        const videoJuego = await getRepository(VideoJuego).findOne(req.params.id);
        if (videoJuego) {
            getRepository(VideoJuego).merge(videoJuego, req.body);
            const results = await getRepository(VideoJuego).save(videoJuego);
            return res.json(results);
        }
        return res.status(404).json({ msg: 'Video juego no encontrado' });
    }

    static getVideoJuegosByEmpresa = async (req: Request, res: Response): Promise<Response> => {
        const empresaDesarrolladora = await getRepository(EmpresaDesarrolladora).findOne(req.params.id, { relations: ['videoJuegos', 'videoJuegos.empresaDesarrolladora', 'videoJuegos.plataforma'] });
        if (!empresaDesarrolladora) {
            res.status(404);
        }
        return res.json(empresaDesarrolladora?.videoJuegos);
    }

    static comprarVideoJuego = async (req: Request, res: Response): Promise<Response> => {
        // debe saber por el mecanismo de autenticacion el id del usuario
        // que está haciendo la peticion, vamos a suponer que es el usuario 3
        const usuarioId = 4;
        const usuario = await getRepository(User).findOne(usuarioId);
        const videoJuego = await getRepository(VideoJuego).findOne(req.body.videoJuegoId);
        let carrito = await getRepository(Factura).findOne({
            where: {
                user: usuarioId,
                despachado: false
            }
        });
        if (!carrito) {
            carrito = new Factura();
            carrito.despachado = false;
        }

        if (usuario) {
            carrito.user = usuario;
        }

        await getRepository(Factura).save(carrito);

        const registro = new Registro();

        registro.cantidad = req.body.cantidad;
        registro.factura = carrito;
        registro.fecha = (new Date()).toISOString().split('T')[0]; // se toma el anio-mes-dia
        if (videoJuego) {
            registro.videoJuego = videoJuego;
            registro.precioTotal = videoJuego.precio * req.body.cantidad;
        }

        // debe buscar si hay carrito creado (factura.despachado = false)
        // si hay carrito, lo usa
        // si no hay carrito, lo crea
        // recupera el videoJuego desde el payload de la req
        // crea un registro y lo asocia con la factura (carrito), el videoJuego y le
        // setea la cantidad y el precioTotal.

        const results = await getRepository(Registro).save(registro);
        return res.json(results);
    }

}


