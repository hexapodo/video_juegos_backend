import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { Factura } from '../entity/Factura';

export class FacturaController {

    static getFacturas = async (req: Request, res: Response): Promise<Response> => {
        const factura = await getRepository(Factura).find({
            relations: [
                'registros',
                'user',
                'registros.videoJuego'
            ]
        });
        return res.json(factura.filter(factura => factura.despachado));
    }

}


