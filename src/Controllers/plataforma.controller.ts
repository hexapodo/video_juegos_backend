import { Request, Response } from 'express'
import { getRepository } from 'typeorm';
import { Plataforma } from '../entity/Plataforma';

export class PlataformaController {

    static getPlataformas = async (req: Request, res: Response): Promise<Response> => {
        const plataforma = await getRepository(Plataforma).find({relations: ['videoJuegos', 'videoJuegos.empresaDesarrolladora']});
        return res.json(plataforma);
    }

}


