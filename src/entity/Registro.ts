import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Factura } from './Factura';
import { VideoJuego } from './VideoJuego';
@Entity()
export class Registro {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('date')
    fecha: string;

    @Column()
    cantidad: number;

    @Column()
    precioTotal: number;

    @ManyToOne(type => VideoJuego, videoJuego => videoJuego.registros)
    videoJuego: VideoJuego;

    @ManyToOne(type => Factura, factura => factura.registros)
    factura: Factura;

}