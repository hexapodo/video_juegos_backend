import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { Registro } from './Registro';
import { User } from './User';
@Entity()
export class Factura {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    despachado: boolean;

    @OneToMany(type => Registro, registro => registro.factura)
    registros: Registro[];

    @ManyToOne(type => User, user => user.facturas)
    user: User;

}